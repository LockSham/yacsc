/*
 * Yacsc (Yet Another CLI Stream Colouriser) - A regular expression cli stream colouriser
 * Copyright (C) 2023  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use regex;
use serde::{Deserialize, Serialize};
use std::ops::Deref;

/***
 * Wrapper struct arround the regex::Regex struct which implements PartialEq
 */
#[derive(Deserialize, Debug, Serialize, Clone)]
pub struct Regex(#[serde(with = "serde_regex")] regex::Regex);

impl Regex {
	pub fn new(pattern: &str) -> Result<Regex, regex::Error> {
		Ok(Regex::from(regex::Regex::new(pattern)?))
	}
}

impl PartialEq for Regex {
	fn eq(&self, other: &Regex) -> bool {
		self.0.as_str() == other.0.as_str()
	}
}
impl Eq for Regex {}

impl From<regex::Regex> for Regex {
	fn from(regex: regex::Regex) -> Self {
		Regex(regex)
	}
}

impl Deref for Regex {
	type Target = regex::Regex;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}
