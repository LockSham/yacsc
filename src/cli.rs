/*
 * Yacsc (Yet Another CLI Stream Colouriser) - A regular expression cli stream colouriser
 * Copyright (C) 2023  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use getopts::Options;

const USAGE: &str =
	"[-c|--config CONFIG] ... [-h|--help] [-s|--schema SCHEMA] [-t|--test-config] [-v|--version] FILES ...";

#[derive(Debug, PartialEq)]
pub struct CliOptions {
	pub name: String,
	pub config: Vec<String>,
	pub schema: Option<String>,
	pub files: Vec<String>,
	pub test_config: bool,
	pub print_version: bool,
}

impl CliOptions {
	pub fn new(args: Vec<String>) -> Result<CliOptions, String> {
		let mut opts = Options::new();
		opts.optmulti("c", "config", "CONFIG", "Config file to read colour schemas from");
		opts.optflag("h", "help", "print this help message");
		opts.optopt("s", "schema", "SCHEMA", "Colour schema to colourise output with");
		opts.optflag("t", "test-config", "test and print configuration");
		opts.optflag("v", "version", "print version");

		if args.is_empty() {
			return Err(opts.usage(&format!("Error: not enough arguments\n\nUsage: {}", USAGE)));
		}

		let name = args[0].clone();
		let usage = |err: &str| opts.usage(&format!("Error: {}\n\nUsage: {} {}", err, name, USAGE));

		let matches = match opts.parse(&args[1..]) {
			Ok(m) => m,
			Err(f) => return Err(usage(&f.to_string())),
		};

		if matches.opt_present("h") {
			return Err(opts.usage(&format!("Usage: {} {}", name, USAGE)));
		}

		Ok(CliOptions {
			name,
			config: matches.opt_strs("c"),
			schema: matches.opt_str("s"),
			test_config: matches.opt_present("t"),
			print_version: matches.opt_present("v"),
			files: matches.free,
		})
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use yare::parameterized;

	fn expected_usage(prog_name: Option<&str>, error: Option<&str>) -> String {
		let error = match error {
			Some(e) => format!("{}\n\n", e),
			None => String::new(),
		};

		let prog_name = match prog_name {
			Some(name) => format!("{} ", name),
			None => String::new(),
		};

		format!(
			"{}Usage: {}[-c|--config CONFIG] ... [-h|--help] [-s|--schema SCHEMA] [-t|--test-config] [-v|--version] FILES ...

Options:
    -c, --config Config file to read colour schemas from
                        CONFIG
    -h, --help          print this help message
    -s, --schema Colour schema to colourise output with
                        SCHEMA
    -t, --test-config   test and print configuration
    -v, --version       print version
",
			error, prog_name
		)
	}

	#[parameterized(
		create_cli_options_with_only_name = {
			vec![
				"test/yacsc".into(),
			],
			Ok(CliOptions{
				name: String::from("test/yacsc"),
				config: Vec::new(),
				schema: None,
				files: Vec::new(),
				test_config: false,
				print_version: false,
			}),
		},
		create_cli_options_with_paremeters = {
			vec![
				"test/yacsc".into(),
				"-s".into(),
				"some-schema".into(),
				"-c".into(),
				"config-1.toml".into(),
				"-c".into(),
				"examples/config-2.toml".into(),
				"-t".into(),
				"file1".into(),
				"examples/file2".into(),
			],
			Ok(CliOptions{
				name: String::from("test/yacsc"),
				config: vec!["config-1.toml".into(), "examples/config-2.toml".into()],
				schema: Some("some-schema".into()),
				files: vec!["file1".into(), "examples/file2".into()],
				test_config: true,
				print_version: false,
			}),
		},
		create_cli_options_with_version_short = {
			vec![
				"test/yacsc".into(),
				"-v".into(),
			],
			Ok(CliOptions{
				name: String::from("test/yacsc"),
				config: Vec::new(),
				schema: None,
				files: Vec::new(),
				test_config: false,
				print_version: true,
			}),
		},
		create_cli_options_with_version_long = {
			vec![
				"test/yacsc".into(),
				"-v".into(),
			],
			Ok(CliOptions{
				name: String::from("test/yacsc"),
				config: Vec::new(),
				schema: None,
				files: Vec::new(),
				test_config: false,
				print_version: true,
			}),
		},
		error_on_no_arguments = {
			Vec::new(),
			Err(expected_usage(None, Some("Error: not enough arguments"))),
		},
		error_on_help_argument = {
			vec!["test/yacsc".into(), "-h".into()],
			Err(expected_usage(Some("test/yacsc"), None)),
		},
		error_on_invalid_arguments = {
			vec!["test/yacsc".into(), "--not-valid".into()],
			Err(expected_usage(Some("test/yacsc"), Some("Error: Unrecognized option: 'not-valid'"))),
		},
	)]
	fn test_cli_options_new(args: Vec<String>, expected: Result<CliOptions, String>) {
		let got = CliOptions::new(args);
		assert_eq!(expected, got);
	}
}
