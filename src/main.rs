/*
 * Yacsc (Yet Another CLI Stream Colouriser) - A regular expression cli stream colouriser
 * Copyright (C) 2023  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use atty::Stream;
use rayon::prelude::*;
use std::{
	fs::File,
	io::{self, BufRead, BufReader, BufWriter, Read, Write},
	path::Path,
	process,
	sync::mpsc::sync_channel,
};

use yacsc::{
	colour,
	state::{State, StateError},
};

fn read_from_reader_parallel<R, W>(state: &mut State, input: R, output: W)
where
	R: Read + Send,
	W: Write + Send,
{
	let (sender, receiver) = sync_channel::<String>(state.config.max_batched_lines);

	rayon::join(
		move || {
			let input = BufReader::new(input);
			for line in input.lines() {
				if let Ok(line) = line {
					if let Err(_) = sender.send(line) {
						return;
					}
				}
			}
		},
		move || {
			let mut output = BufWriter::new(output);
			loop {
				let mut batch = vec![];
				batch.push(match receiver.recv() {
					Ok(l) => l,
					Err(_) => break,
				});

				for _ in 1..state.config.max_batched_lines {
					batch.push(match receiver.try_recv() {
						Ok(l) => l,
						Err(_) => break,
					});
				}

				let output_lines: Vec<Vec<u8>> = batch
					.par_iter()
					.map(|line| {
						let mut outline = Vec::new();
						colour::put_colourised_string_output(&state.get_colour_schema(), &line, &mut outline)
							.unwrap_or_else(|err| {
								eprintln!("{}", err);
								process::exit(1);
							});
						outline.write(b"\n").unwrap();

						outline
					})
					.collect();

				for line in output_lines {
					output.write_all(&line).unwrap_or_else(|err| {
						eprintln!("Error: writing: {}", err);
						process::exit(1);
					});
				}
				output.flush().unwrap();
			}
		},
	);
}

fn read_from_stdin<W>(state: &mut State, output: &mut W)
where
	W: io::Write + Send,
{
	read_from_reader_parallel(state, io::stdin(), output);
}

fn read_from_files<W>(state: &mut State, output: &mut W)
where
	W: io::Write + Send,
{
	(&state.options.files).into_iter().for_each(|file| {
		match Path::new(file).try_exists() {
			Ok(result) => {
				if !result {
					eprintln!("{}: file not found", file);
					process::exit(1);
				}
			}
			Err(e) => {
				eprintln!("unable to read file {}: {}", file, e);
				process::exit(1);
			}
		};
	});

	for filename in state.options.files.clone() {
		let file = File::open(&filename).unwrap_or_else(|err| {
			eprintln!("unable to open {}: {}", filename, err);
			process::exit(1);
		});

		read_from_reader_parallel(state, file, &mut *output);
	}
}

fn main() {
	let output = &mut io::stdout();

	let mut state = State::new().unwrap_or_else(|err| match err {
		StateError::Informational(info) => {
			println!("{}", info);
			process::exit(0);
		}
		StateError::Error(err) => {
			eprintln!("{}", err);
			process::exit(2);
		}
	});

	if state.options.test_config {
		match state.config.to_sorted_yaml() {
			Ok(toml) => {
				if atty::is(Stream::Stdout) {
					colour::put_colourised_string_output(&state.config.get_config_colour_schema(), &toml, output)
						.unwrap_or_else(|err| {
							eprintln!("{}", err);
							process::exit(1);
						});
				} else {
					print!("{}", &toml);
				}
			}
			Err(e) => {
				eprintln!("failed to serialize config: {}", e);
				process::exit(2);
			}
		};
		return;
	}

	if state.options.files.len() > 0 {
		read_from_files(&mut state, output);
	} else {
		read_from_stdin(&mut state, output);
	}
}
