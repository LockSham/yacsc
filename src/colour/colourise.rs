/*
 * Yacsc (Yet Another CLI Stream Colouriser) - A regular expression cli stream colouriser
 * Copyright (C) 2023  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use ansi_term::Style;
use rayon::prelude::*;
use regex::RegexSet;
use std::{io, ops::Range};

use crate::colour::schema::{ColourSchema, RegexColour};

pub fn put_colourised_string_output<W>(
	colour_schema: &ColourSchema,
	input: &str,
	output: &mut W,
) -> Result<(), String>
where
	W: io::Write,
{
	let styles = match get_colour_styles(colour_schema, input) {
		Ok(m) => m,
		Err(e) => return Err(format!("failed to get colour matches {}", e)),
	};

	write_coloured_output_from_styles(input, &styles, output)
}

fn write_coloured_output_from_styles<W>(input: &str, styles: &Vec<Style>, output: &mut W) -> Result<(), String>
where
	W: io::Write,
{
	let mut start_idx = 0;
	let mut current_style = &Style::default();
	let mut last_idx = 0;

	if input.len() != styles.len() {
		return Err(String::from("input and styles are not of the same length"));
	}

	for (idx, style) in styles.iter().enumerate() {
		if idx == 0 {
			current_style = style;
		}

		if style == current_style {
			continue;
		}

		if let Err(err) = write!(output, "{}", current_style.paint(&input[start_idx..idx])) {
			return Err(format!("Failed to write coloured output: {}", err.to_string()));
		}
		start_idx = idx;
		last_idx = idx;
		current_style = style;
	}

	if input.len() > 0 && last_idx < input.len() {
		if let Err(err) = write!(output, "{}", current_style.paint(&input[start_idx..])) {
			return Err(format!("Failed to write coloured output: {}", err.to_string()));
		}
	}

	Ok(())
}

fn get_colour_styles(colour_schema: &ColourSchema, input: &str) -> Result<Vec<Style>, String> {
	let set = match RegexSet::new(colour_schema.iter().map(|r| r.pattern.as_str()).collect::<Vec<&str>>()) {
		Ok(v) => v,
		Err(err) => return Err(err.to_string()),
	};

	let mut styles = vec![Style::default(); input.len()];
	let mut regex_matches: Vec<(usize, Vec<(Range<usize>, Style)>)> = set
		.matches(&input)
		.iter()
		.par_bridge()
		.map(|reg_match| {
			let regex = &colour_schema[reg_match];
			let mut styles: Vec<(Range<usize>, Style)> = Vec::new();

			match &regex.colour {
				RegexColour::Full(colour) => {
					for m in regex.pattern.find_iter(&input) {
						styles.push((m.start()..m.end(), *colour));
					}
				}
				RegexColour::Mapped(colours) => {
					for caps in regex.pattern.captures_iter(&input) {
						for (key, colour) in colours {
							if let Some(m) = caps.name(key) {
								styles.push((m.start()..m.end(), *colour));
							}
						}
					}
				}
			};

			(reg_match, styles)
		})
		.collect();

	regex_matches.sort_by(|a, b| b.0.cmp(&a.0));
	for (_, regex_match) in regex_matches {
		for (m, colour) in regex_match {
			for i in m.start..m.end {
				styles[i] = colour;
			}
		}
	}

	Ok(styles)
}

#[cfg(test)]
mod tests {
	use ansi_term::Colour;
	use std::collections::HashMap;
	use yare::parameterized;

	use super::*;
	use crate::colour::schema::{ColourSchema, ColourSchemaRegex, RegexColour};

	use crate::regex::Regex;

	#[parameterized(
		no_colour = {
			ColourSchema::from(vec![
				ColourSchemaRegex{
					pattern: Regex::new("info").unwrap(),
					colour: RegexColour::Full(Colour::RGB(0, 0, 255).normal())
				}
			]),
			"a test string that should not end up coloured",
			"a test string that should not end up coloured"
		},
		regex_string_colour = {
			ColourSchema::from(vec![
				ColourSchemaRegex{
					pattern: Regex::new("info").unwrap(),
					colour: RegexColour::Full(Colour::RGB(0, 0, 255).normal())
				}
			]),
			"an info string where info is coloured",
			&format!(
				"an {} string where {} is coloured",
				Colour::RGB(0, 0, 255).paint("info"),
				Colour::RGB(0, 0, 255).paint("info"),
			),
		},
		regex_mapped_colour = {
			ColourSchema::from(vec![
				ColourSchemaRegex{
					pattern: Regex::new("^(?P<timestamp>[^ ]+) (?P<daemon>[a-zA-Z0-9-_]*) (?P<metadata>(?:\\[(.*?)\\] )*)").unwrap(),
					colour: RegexColour::Mapped(HashMap::from([
						("timestamp".into(), Colour::Green.normal()),
						("daemon".into(), Colour::Purple.normal()),
						("metadata".into(), Colour::RGB(18, 69, 103).normal()),
					])),
				},
			]),
			"2023-04-16T14:00:14+10:00 test-daemon [args: -v --test] [init] listening on 0.0.0.0:80",
			&format!(
				"{} {} {}listening on 0.0.0.0:80",
				Colour::Green.paint("2023-04-16T14:00:14+10:00"),
				Colour::Purple.paint("test-daemon"),
				Colour::RGB(18, 69, 103).paint("[args: -v --test] [init] "),
			),
		},
		multiple_regexes = {
			ColourSchema::from(vec![
				ColourSchemaRegex{
					pattern: Regex::new("^[^ ]+").unwrap(),
					colour: RegexColour::Full(Colour::Green.normal())
				},
				ColourSchemaRegex{
					pattern: Regex::new("^(?:[^ ]+) (?P<daemon>[a-zA-Z0-9-_]+)").unwrap(),
					colour: RegexColour::Mapped(HashMap::from([
						("daemon".into(), Colour::Purple.normal()),
					])),
				},
				ColourSchemaRegex{
					pattern: Regex::new("\\[(?P<metadata>.*?)\\]").unwrap(),
					colour: RegexColour::Mapped(HashMap::from([
						("metadata".into(), Colour::RGB(18, 69, 103).normal()),
					])),
				},
			]),
			"2023-04-16T14:00:14+10:00 test-daemon [args: -v --test] [init] listening on 0.0.0.0:80",
			&format!(
				"{} {} [{}] [{}] listening on 0.0.0.0:80",
				Colour::Green.paint("2023-04-16T14:00:14+10:00"),
				Colour::Purple.paint("test-daemon"),
				Colour::RGB(18, 69, 103).paint("args: -v --test"),
				Colour::RGB(18, 69, 103).paint("init"),
			),
		},
		overlapping_schemas = {
			ColourSchema::from(vec![
				ColourSchemaRegex{
					pattern: Regex::new("Hello world").unwrap(),
					colour: RegexColour::Full(Colour::Red.normal())
				},
				ColourSchemaRegex{
					pattern: Regex::new("Hello world this is a string").unwrap(),
					colour: RegexColour::Full(Colour::Green.normal())
				},
				ColourSchemaRegex{
					pattern: Regex::new("string that overlaps").unwrap(),
					colour: RegexColour::Full(Colour::Blue.normal())
				},
			]),
			"Hello world this is a string that overlaps",
			&format!(
				"{}{}{}",
				Colour::Red.paint("Hello world"),
				Colour::Green.paint(" this is a string"),
				Colour::Blue.paint(" that overlaps"),
			),
		},
		duplicate_regex_schemas = {
			ColourSchema::from(vec![
				ColourSchemaRegex{
					pattern: Regex::new("[0-9]+(\\.[0-9]+)*").unwrap(),
					colour: RegexColour::Full(Colour::Red.normal())
				},
				ColourSchemaRegex{
					pattern: Regex::new("[0-9]+(\\.[0-9]+)*").unwrap(),
					colour: RegexColour::Full(Colour::Red.normal())
				},
			]),
			"1000 10098764.00988776",
			&format!(
				"{} {}",
				Colour::Red.paint("1000"),
				Colour::Red.paint("10098764.00988776"),
			),
		},
		regex_inbetween_regex = {
			ColourSchema::from(vec![
				ColourSchemaRegex{
					pattern: Regex::new("error").unwrap(),
					colour: RegexColour::Full(Colour::Red.normal())
				},
				ColourSchemaRegex{
					pattern: Regex::new("'(?:[^'\\\\]|\\.)*'").unwrap(),
					colour: RegexColour::Full(Colour::Purple.normal()),
				},
			]),
			"'A quoted error string' with errors",
			&format!(
				"{}{}{} with {}s",
				Colour::Purple.paint("'A quoted "),
				Colour::Red.paint("error"),
				Colour::Purple.paint(" string'"),
				Colour::Red.paint("error"),
			),
		},
		regex_full_match = {
			ColourSchema::from(vec![
				ColourSchemaRegex{
					pattern: Regex::new("'(?:[^'\\\\]|\\.)*'").unwrap(),
					colour: RegexColour::Full(Colour::Purple.normal()),
				},
			]),
			"'A quoted string'",
			&format!(
				"{}",
				Colour::Purple.paint("'A quoted string'"),
			),
		},
		styled_colour = {
			ColourSchema::from(vec![
				ColourSchemaRegex{
					pattern: Regex::new("info").unwrap(),
					colour: RegexColour::Full(Colour::RGB(0, 0, 255).bold().blink())
				}
			]),
			"an info string where info is coloured",
			&format!(
				"an {} string where {} is coloured",
				Colour::RGB(0, 0, 255).bold().blink().paint("info"),
				Colour::RGB(0, 0, 255).bold().blink().paint("info"),
			),
		},
	)]
	fn test_put_colourised_string_output(schema: ColourSchema, input: &str, expected: &str) {
		let mut got: Vec<u8> = Vec::new();
		let result = put_colourised_string_output(&schema, input, &mut got);
		//println!("expected:\t{}", expected);
		//println!("got:\t\t{}", std::str::from_utf8(&got).unwrap());
		assert_eq!(true, result.is_ok());
		assert_eq!(expected, std::str::from_utf8(&got).unwrap());
	}
}
