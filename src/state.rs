/*
 * Yacsc (Yet Another CLI Stream Colouriser) - A regular expression cli stream colouriser
 * Copyright (C) 2023  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use dirs;
use std::{env, fs, io, path::Path};

use crate::{cli::CliOptions, colour::ColourSchema, config::Config};

pub enum StateError {
	Informational(String),
	Error(String),
}

pub struct State {
	pub options: CliOptions,
	pub config: Config,
	pub schema: String,
}

fn get_files_from_dir(dir: &str) -> io::Result<Vec<String>> {
	let mut entries = fs::read_dir(dir)?
		.filter_map(|entry| {
			let e = match entry {
				Ok(e) => e,
				Err(_) => return None,
			};

			let file_type = match e.file_type() {
				Ok(t) => t,
				Err(_) => return None,
			};

			if file_type.is_file() {
				if let Some(s) = e.path().to_str() {
					return Some(String::from(s));
				}
			}

			None
		})
		.collect::<Vec<String>>();
	entries.sort();

	Ok(entries)
}

fn get_config_files(options: &CliOptions) -> Vec<String> {
	if options.config.len() != 0 {
		return options.config.clone();
	}

	let mut files: Vec<String> = Vec::new();
	if Path::new("/etc/yacsc/yacsc.toml").exists() {
		files.push(String::from("/etc/yacsc/yacsc.toml"));
	}

	if let Ok(mut system_files) = get_files_from_dir("/etc/yacsc/yacsc.d") {
		files.append(&mut system_files);
	}

	if let Some(user_config_dir) = dirs::config_dir() {
		if let Some(user_config_dir) = user_config_dir.to_str() {
			let user_config = format!("{}/yacsc/yacsc.toml", user_config_dir);
			if Path::new(&user_config).exists() {
				files.push(user_config);
			}

			if let Ok(mut user_files) = get_files_from_dir(&format!("{}/yacsc/yacsc.d", user_config_dir)) {
				files.append(&mut user_files);
			}
		}
	}

	files
}

impl State {
	pub fn new() -> Result<State, StateError> {
		let options = match CliOptions::new(env::args().collect()) {
			Ok(o) => o,
			Err(e) => return Err(StateError::Error(e)),
		};

		if options.print_version {
			return Err(StateError::Informational(format!(
				"yacsc {}",
				env!("YACSC_BUILD_VERSION")
			)));
		}

		let config_files = get_config_files(&options);
		let config = match Config::new(&config_files) {
			Ok(c) => c,
			Err(e) => return Err(StateError::Error(format!("unable to read config: {}", e))),
		};

		let schema = if let Some(cli_schema) = &options.schema {
			cli_schema.clone()
		} else if let Some(default_schema) = &config.default_schema {
			default_schema.clone()
		} else {
			return Err(StateError::Error("no colour schema defined".into()));
		};

		if let Err(_) = config.get_colour_schema(&schema) {
			return Err(StateError::Error(format!(
				"colour schema {} not found in config",
				schema
			)));
		};

		Ok(State {
			options,
			config,
			schema,
		})
	}

	pub fn get_colour_schema(&self) -> ColourSchema {
		self.config.get_colour_schema(&self.schema).unwrap()
	}
}
