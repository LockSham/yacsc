/*
 * Yacsc (Yet Another CLI Stream Colouriser) - A regular expression cli stream colouriser
 * Copyright (C) 2023  Lachlan Shambrook
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use ansi_term::{Colour, Style};
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, fs, num::ParseIntError};

use crate::{
	colour::{ColourSchema, ColourSchemaRegex, RegexColour},
	regex::Regex,
};

#[derive(Deserialize, Debug, Serialize, PartialEq, Eq, PartialOrd, Ord, Clone)]
#[serde(untagged)]
enum ConfigColour {
	Styled {
		fg: String,
		bg: Option<String>,
		bold: Option<bool>,
		dimmed: Option<bool>,
		italic: Option<bool>,
		underline: Option<bool>,
		blink: Option<bool>,
		reverse: Option<bool>,
		hidden: Option<bool>,
		strikethrough: Option<bool>,
	},
	Normal(String),
}

impl From<&str> for ConfigColour {
	fn from(colour: &str) -> Self {
		ConfigColour::Normal(String::from(colour))
	}
}

impl ConfigColour {
	fn get_style(&self, colours: &HashMap<String, ConfigColour>) -> Result<Style, String> {
		let get_colour = |colour: &str, colours: &HashMap<String, ConfigColour>| match colour {
			term_colour if term_colour.starts_with("term_") => match colour_from_term_colour(colour) {
				Some(c) => Ok(c.normal()),
				None => Err(format!("invalid term colour '{}'", colour)),
			},
			hex_colour if hex_colour.starts_with("#") && hex_colour.len() == 7 => {
				match colour_from_hex_colour(colour) {
					Ok(c) => Ok(c.normal()),
					Err(e) => Err(format!("invalid hex colour '{}': {}", colour, e)),
				}
			}
			colour => match colours.get(colour) {
				Some(c) => Ok(c.get_style(&HashMap::new())?),
				None => Err(format!("invalid colour '{}'", colour)),
			},
		};

		match self {
			ConfigColour::Normal(s) => match get_colour(s, colours) {
				Ok(c) => Ok(c),
				Err(e) => Err(e),
			},
			ConfigColour::Styled {
				fg,
				bg,
				bold,
				dimmed,
				italic,
				underline,
				blink,
				reverse,
				hidden,
				strikethrough,
			} => {
				let fg = get_colour(fg, colours)?;

				let is_styled = |value: &Option<bool>| match value {
					Some(b) => *b,
					None => false,
				};
				Ok(Style {
					foreground: fg.foreground,
					background: match bg {
						Some(bg) => match get_colour(bg, colours) {
							Ok(s) => s.foreground,
							Err(e) => return Err(e),
						},
						None => None,
					},
					is_bold: is_styled(bold),
					is_dimmed: is_styled(dimmed),
					is_italic: is_styled(italic),
					is_underline: is_styled(underline),
					is_blink: is_styled(blink),
					is_reverse: is_styled(reverse),
					is_hidden: is_styled(hidden),
					is_strikethrough: is_styled(strikethrough),
				})
			}
		}
	}
}

#[derive(Deserialize, Debug, Serialize, PartialEq, Clone)]
#[serde(untagged)]
enum ConfigRegexColour {
	String(ConfigColour),
	Mapped(HashMap<String, ConfigColour>),
}

#[derive(Deserialize, Debug, Serialize, PartialEq, Clone)]
struct ConfigColourSchemaRegex {
	pattern: Regex,
	colour: ConfigRegexColour,
}

#[derive(Deserialize, Debug, Serialize, PartialEq, Clone)]
#[serde(tag = "type")]
enum ConfigColourSchema {
	#[serde(alias = "surround")]
	Surround {
		start: char,
		end: char,
		escape: Option<char>,
		colour: ConfigColour,
	},
	#[serde(alias = "regex")]
	Regex(ConfigColourSchemaRegex),
}

#[derive(Deserialize, Debug, Serialize, PartialEq)]
pub struct Config {
	pub default_schema: Option<String>,
	#[serde(default = "default_max_batched_lines")]
	pub max_batched_lines: usize,
	#[serde(default)]
	colours: HashMap<String, ConfigColour>,
	#[serde(default)]
	schemas: HashMap<String, Vec<ConfigColourSchema>>,
}

fn default_max_batched_lines() -> usize {
	1000
}

impl Config {
	pub fn new(files: &Vec<String>) -> Result<Config, String> {
		let mut value = match toml::Value::try_from(get_built_in_config()) {
			Ok(v) => v,
			Err(e) => return Err(format!("unable to create config: {}", e)),
		};

		for filename in files {
			let data = match fs::read_to_string(&filename) {
				Ok(data) => data,
				Err(e) => return Err(format!("unable to read config file {}: {}", filename, e)),
			};

			let file_value: toml::Value = match toml::from_str(&data) {
				Ok(c) => c,
				Err(e) => return Err(format!("unable to parse config file {}: {}", filename, e,)),
			};
			merge_value(&mut value, &file_value);
		}

		let config = match value.try_into::<Config>() {
			Ok(file_config) => file_config,
			Err(e) => return Err(format!("unable to parse config: {}", e,)),
		};

		if let Err(e) = validate_config_colours(&config) {
			return Err(e);
		}

		Ok(config)
	}

	pub fn get_colour_schema(&self, schema: &str) -> Result<ColourSchema, String> {
		let config_schemas = match self.schemas.get(schema) {
			Some(c) => c,
			None => return Err(format!("colour schema {} not found in config", schema)),
		};

		let mut colour_schemas = Vec::new();
		for config_schema in config_schemas {
			let colour_schema = match config_schema {
				ConfigColourSchema::Surround {
					start,
					end,
					escape,
					colour,
				} => {
					let start = regex::escape(&String::from(*start));
					let end = regex::escape(&String::from(*end));

					ColourSchemaRegex {
						pattern: match *escape {
							Some(escape) => {
								let escape = regex::escape(&String::from(escape));

								let regex =
									Regex::new(&format!("{}(?:[^{}{}]|{}.)*{}", start, end, escape, escape, end,));

								match regex {
									Ok(r) => r,
									Err(e) => return Err(format!("Unable to compile surround regex: {}", e)),
								}
							}
							None => {
								let regex = Regex::new(&format!("{}[^{}]*?{}", start, end, end,));

								match regex {
									Ok(r) => r,
									Err(e) => return Err(format!("Unable to compile surround regex: {}", e)),
								}
							}
						},
						colour: RegexColour::Full(colour.get_style(&self.colours)?),
					}
				}
				ConfigColourSchema::Regex(regex_colour) => ColourSchemaRegex {
					pattern: regex_colour.pattern.clone().into(),
					colour: match &regex_colour.colour {
						ConfigRegexColour::String(colour) => RegexColour::Full(colour.get_style(&self.colours)?),
						ConfigRegexColour::Mapped(map) => RegexColour::Mapped(
							map.iter()
								.map(|(key, colour)| {
									Ok((
										key.clone(),
										match colour.get_style(&self.colours) {
											Ok(c) => c,
											Err(e) => return Err(format!("{} for regex mapped key '{}'", e, key)),
										},
									))
								})
								.collect::<Result<HashMap<String, Style>, String>>()?,
						),
					},
				},
			};

			colour_schemas.push(colour_schema);
		}

		Ok(colour_schemas.into())
	}

	pub fn get_config_colour_schema(&self) -> ColourSchema {
		let mut colours: Vec<&ConfigColour> = Vec::new();
		for (_, schemas) in self.schemas.iter() {
			for schema in schemas {
				match schema {
					ConfigColourSchema::Surround {
						start: _,
						end: _,
						escape: _,
						colour,
					} => colours.push(&colour),
					ConfigColourSchema::Regex(r) => match &r.colour {
						ConfigRegexColour::String(s) => colours.push(&s),
						ConfigRegexColour::Mapped(map) => {
							for colour in map.values() {
								colours.push(&colour)
							}
						}
					},
				};
			}
		}

		for (_, colour) in self.colours.iter() {
			colours.push(&colour);
		}

		let styled_regex = Regex::new("(?m)^((fg|bg)\\s*=\\s*)(.*)$").unwrap();

		colours.sort();
		let mut colour_schemas = colours
			.iter()
			.map(|colour| match colour {
				ConfigColour::Normal(s) => ColourSchemaRegex {
					pattern: Regex::new(&format!("\"{}\"", s)).unwrap(),
					colour: RegexColour::Full(colour.get_style(&self.colours).unwrap()),
				},
				ConfigColour::Styled {
					fg: _,
					bg: _,
					bold: _,
					dimmed: _,
					italic: _,
					underline: _,
					blink: _,
					reverse: _,
					hidden: _,
					strikethrough: _,
				} => {
					let escaped_toml = regex::escape(&toml::to_string(colour).unwrap());
					let regex_str = styled_regex.replace_all(&escaped_toml, |caps: &regex::Captures| {
						format!("{}(?P<{}>{})", &caps[1], &caps[2], &caps[3])
					});

					ColourSchemaRegex {
						pattern: Regex::new(&regex_str).unwrap(),
						colour: RegexColour::Mapped(HashMap::from([
							("fg".into(), colour.get_style(&self.colours).unwrap()),
							("bg".into(), colour.get_style(&self.colours).unwrap()),
						])),
					}
				}
			})
			.collect::<Vec<ColourSchemaRegex>>();

		colour_schemas.push(ColourSchemaRegex {
			pattern: Regex::new(
				"(?x)
				(?P<key>[a-zA-Z0-9_.-]+)\\s=\\s[\\[{]?(?:
					(?P<number>[0-9.+-]+)|
					(?P<bool>(true|false))|
					(?P<string>['\"].+['\"]
				)|[\\[{])[\\]}]?[\\\n,]
			",
			)
			.unwrap(),
			colour: RegexColour::Mapped(HashMap::from([
				("key".into(), Colour::Red.into()),
				("number".into(), Colour::Yellow.into()),
				("bool".into(), Colour::Blue.into()),
				("string".into(), Colour::Purple.into()),
			])),
		});

		colour_schemas.into()
	}

	pub fn to_sorted_yaml(&self) -> Result<String, toml::ser::Error> {
		// Abuse the fact that by default toml::Table is backed by BTreeMap
		// and will thus be ordered
		toml::to_string_pretty(&toml::Value::try_from(self)?)
	}
}

fn get_built_in_config() -> Config {
	Config {
		default_schema: Some(String::from("yacsc-default")),
		max_batched_lines: default_max_batched_lines(),
		colours: HashMap::new(),
		schemas: HashMap::from([(
			"yacsc-default".into(),
			vec![
				ConfigColourSchema::Regex(ConfigColourSchemaRegex {
					pattern: Regex::new("[0-9]+(\\.[0-9]+)*").unwrap(),
					colour: ConfigRegexColour::String("term_yellow".into()),
				}),
				ConfigColourSchema::Regex(ConfigColourSchemaRegex {
					pattern: Regex::new("\"(?:[^\"\\\\]|\\\\.)*\"").unwrap(),
					colour: ConfigRegexColour::String("term_magenta".into()),
				}),
				ConfigColourSchema::Regex(ConfigColourSchemaRegex {
					pattern: Regex::new("'(?:[^'\\\\]|\\\\.)*'").unwrap(),
					colour: ConfigRegexColour::String("term_magenta".into()),
				}),
			],
		)]),
	}
}

fn validate_config_colours(config: &Config) -> Result<(), String> {
	for (key, colour) in config.colours.iter() {
		if let Err(e) = colour.get_style(&HashMap::new()) {
			return Err(format!("invalid colour '{}': {}", key, e));
		}
	}

	for (schema, _) in config.schemas.iter() {
		if let Err(e) = config.get_colour_schema(&schema) {
			return Err(format!("invalid schema '{}': {}", schema, e));
		}
	}

	Ok(())
}

fn colour_from_hex_colour(hex_colour: &str) -> Result<Colour, ParseIntError> {
	if hex_colour.starts_with("#") && hex_colour.len() == 7 {
		return Ok(Colour::RGB(
			u8::from_str_radix(&hex_colour[1..3], 16)?,
			u8::from_str_radix(&hex_colour[3..5], 16)?,
			u8::from_str_radix(&hex_colour[5..7], 16)?,
		));
	}

	Ok(Colour::Black)
}

fn colour_from_term_colour(colour: &str) -> Option<Colour> {
	match colour {
		"term_black" => Some(Colour::Black),
		"term_red" => Some(Colour::Red),
		"term_green" => Some(Colour::Green),
		"term_yellow" => Some(Colour::Yellow),
		"term_blue" => Some(Colour::Blue),
		"term_magenta" => Some(Colour::Purple),
		"term_cyan" => Some(Colour::Cyan),
		"term_white" => Some(Colour::White),
		"term_black_alt" => Some(Colour::Fixed(8)),
		"term_red_alt" => Some(Colour::Fixed(9)),
		"term_green_alt" => Some(Colour::Fixed(10)),
		"term_yellow_alt" => Some(Colour::Fixed(11)),
		"term_blue_alt" => Some(Colour::Fixed(12)),
		"term_magenta_alt" => Some(Colour::Fixed(13)),
		"term_cyan_alt" => Some(Colour::Fixed(14)),
		"term_white_alt" => Some(Colour::Fixed(15)),
		_ => None,
	}
}

fn merge_value(target: &mut toml::Value, source: &toml::Value) {
	match source {
		toml::Value::String(s) => {
			*target = toml::Value::String(s.clone());
		}
		toml::Value::Integer(i) => {
			*target = toml::Value::Integer(*i);
		}
		toml::Value::Float(f) => {
			*target = toml::Value::Float(*f);
		}
		toml::Value::Boolean(b) => {
			*target = toml::Value::Boolean(*b);
		}
		toml::Value::Datetime(d) => {
			*target = toml::Value::Datetime(d.clone());
		}
		toml::Value::Array(array) => match target.as_array_mut() {
			Some(target_array) => {
				for value in array {
					target_array.push(value.clone());
				}
			}
			None => {
				*target = toml::Value::Array(array.clone());
			}
		},
		toml::Value::Table(table) => match target.as_table_mut() {
			Some(target_table) => {
				for (k, v) in table {
					match target_table.get_mut(k) {
						Some(target_value) => {
							merge_value(target_value, v);
						}
						None => {
							target_table.insert(k.clone(), v.clone());
						}
					}
				}
			}
			None => {
				*target = toml::Value::Table(table.clone());
			}
		},
	};
}

#[cfg(test)]
mod tests {
	use super::*;
	use std::collections::HashMap;
	use yare::parameterized;

	use crate::{
		colour::{ColourSchema, ColourSchemaRegex, RegexColour},
		regex::Regex,
	};

	#[parameterized(
		create_config_with_single_file = {
			vec![
				"examples/example.toml".into(),
			],
			Ok(Config{
				default_schema: Some(String::from("simple")),
				max_batched_lines: default_max_batched_lines(),
				colours: HashMap::from([
					("info".into(), "#1DEDCD".into()),
					("warn".into(), "#ffa500".into()),
					("red".into(), "#FF0000".into()),
				]),
				schemas: HashMap::from([
					("yacsc-default".into(), get_built_in_config().schemas["yacsc-default"].clone()),
					(
						"simple".into(),
						vec![
							ConfigColourSchema::Surround{
								start: '"',
								end: '"',
								escape: Some('\\'),
								colour: "term_magenta".into(),
							},
							ConfigColourSchema::Surround{
								start: '\'',
								end: '\'',
								escape: None,
								colour: "info".into(),
							},
							ConfigColourSchema::Regex(ConfigColourSchemaRegex{
								pattern: Regex::new("test").unwrap(),
								colour: ConfigRegexColour::String("info".into()),
							}),
							ConfigColourSchema::Regex(ConfigColourSchemaRegex{
								pattern: Regex::new("strings").unwrap(),
								colour: ConfigRegexColour::String("term_yellow".into()),
							}),
							ConfigColourSchema::Regex(ConfigColourSchemaRegex{
								pattern: Regex::new(" in ").unwrap(),
								colour: ConfigRegexColour::String("red".into()),
							}),
						],
					),
				]),
			}),
		},
		create_config_with_multiple_files = {
			vec![
				"examples/example.toml".into(),
				"examples/example_additions.toml".into(),
			],
			Ok(Config{
				default_schema: Some(String::from("regex-example")),
				max_batched_lines: default_max_batched_lines(),
				colours: HashMap::from([
					("info".into(), "#1DEDCD".into()),
					("red".into(), "#FF0000".into()),
					("warn".into(), "#ffa501".into()),
				]),
				schemas: HashMap::from([
					("yacsc-default".into(), get_built_in_config().schemas["yacsc-default"].clone()),
					(
						"simple".into(),
						vec![
							ConfigColourSchema::Surround{
								start: '"',
								end: '"',
								escape: Some('\\'),
								colour: "term_magenta".into(),
							},
							ConfigColourSchema::Surround{
								start: '\'',
								end: '\'',
								escape: None,
								colour: "info".into(),
							},
							ConfigColourSchema::Regex(ConfigColourSchemaRegex{
								pattern: Regex::new("test").unwrap(),
								colour: ConfigRegexColour::String("info".into()),
							}),
							ConfigColourSchema::Regex(ConfigColourSchemaRegex{
								pattern: Regex::new("strings").unwrap(),
								colour: ConfigRegexColour::String("term_yellow".into()),
							}),
							ConfigColourSchema::Regex(ConfigColourSchemaRegex{
								pattern: Regex::new(" in ").unwrap(),
								colour: ConfigRegexColour::String("red".into()),
							}),
						],
					),
					(
						"regex-example".into(),
						vec![
							ConfigColourSchema::Regex(ConfigColourSchemaRegex{
								pattern: Regex::new("[0-9]+(\\.[0-9]+)*").unwrap(),
								colour: ConfigRegexColour::String("term_yellow".into()),
							}),
							ConfigColourSchema::Regex(ConfigColourSchemaRegex{
								pattern: Regex::new("\"(?:[^\"\\\\]|\\\\.)*\"").unwrap(),
								colour: ConfigRegexColour::String("info".into()),
							}),
							ConfigColourSchema::Regex(ConfigColourSchemaRegex{
								pattern: Regex::new("\\berror\\b").unwrap(),
								colour: ConfigRegexColour::String("red".into()),
							}),
							ConfigColourSchema::Regex(ConfigColourSchemaRegex{
								pattern: Regex::new("\\binfo\\b").unwrap(),
								colour: ConfigRegexColour::String("info".into()),
							}),
							ConfigColourSchema::Regex(ConfigColourSchemaRegex{
								pattern: Regex::new("\\bwarn\\b").unwrap(),
								colour: ConfigRegexColour::String("warn".into()),
							}),
							ConfigColourSchema::Regex(ConfigColourSchemaRegex{
								pattern: Regex::new("(?P<usage>Usage:)\\s(?P<command>[^\\s]+)\\s(?P<options>(?:\\[.*?\\]\\s?)*)\\s(?P<positionals>(?:[A-Z]+(?:\\.\\.\\.)?)*)").unwrap(),
								colour: ConfigRegexColour::Mapped(HashMap::from([
									(
										"usage".into(),
										ConfigColour::Styled {
											fg: "info".into(),
											bg: None,
											bold: Some(true),
											dimmed: None,
											italic: None,
											underline: None,
											blink: None,
											reverse: None,
											hidden: None,
											strikethrough: None
										}
									),
									("command".into(), "term_green".into()),
									("options".into(), "term_blue".into()),
									("positionals".into(), "term_cyan".into()),
								])),
							}),
						],
					),
				]),
			}),
		},
		fail_to_create_config_with_invalid_regex = {
			vec![
				"examples/example_failure.toml".into(),
			],
			Err("unable to parse config: regex parse error:\n    ([0-9]*.[0-9]\n    ^\nerror: unclosed group\nin `schemas.regex-failure`\n".into()),
		},
		fail_to_create_config_with_non_existant_file = {
			vec![
				"examples/does-not-exist.toml".into(),
			],
			Err("unable to read config file examples/does-not-exist.toml: No such file or directory (os error 2)".into()),
		},
	)]
	fn test_config_new(files: Vec<String>, expected: Result<Config, String>) {
		let got = Config::new(&files);
		assert_eq!(expected, got);
	}

	#[test]
	fn test_get_colour_schema() {
		let config = Config::new(&vec!["examples/example.toml".into()]).unwrap();
		assert_eq!(
			Ok(ColourSchema::from(vec![
				ColourSchemaRegex {
					pattern: Regex::new("\"(?:[^\"\\\\]|\\\\.)*\"").unwrap(),
					colour: RegexColour::Full(Colour::Purple.normal()),
				},
				ColourSchemaRegex {
					pattern: Regex::new("'[^']*?'").unwrap(),
					colour: RegexColour::Full(Colour::RGB(29, 237, 205).normal()),
				},
				ColourSchemaRegex {
					pattern: Regex::new("test").unwrap(),
					colour: RegexColour::Full(Colour::RGB(29, 237, 205).normal()),
				},
				ColourSchemaRegex {
					pattern: Regex::new("strings").unwrap(),
					colour: RegexColour::Full(Colour::Yellow.normal()),
				},
				ColourSchemaRegex {
					pattern: Regex::new(" in ").unwrap(),
					colour: RegexColour::Full(Colour::RGB(255, 0, 0).normal()),
				},
			])),
			config.get_colour_schema("simple")
		);
	}
}
