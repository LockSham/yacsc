# Yet Another CLI Stream Colouriser (yacsc)
## About
yacsc is a line oriented cli stream colouriser tool that takes input and colourises it using regular expressions

## Usage
```
Usage: yacsc [-c|--config CONFIG] ... [-h|--help] [-s|--schema SCHEMA] [-t|--test-config] [-v|--version] FILES ...

Options:
    -c, --config Config file to read colour schemas from
                        CONFIG
    -h, --help          print this help message
    -s, --schema Colour schema to colourise output with
                        SCHEMA
    -t, --test-config   test and print configuration
    -v, --version       print version
```

## Configuration
Yacsc is configured by toml files read from system level or user level locations or at run time via the `--config` command argument.

System level configuration are read from the following locations:
- `/etc/yacsc/yacsc.toml`
- `/etc/yacsc/yacsc.d/*` in alphabetical order

User level configurations are read from the following locations:
- `${XDG_CONFIG_HOME:-$HOME/.config}/yacsc/yacsc.toml`
- `${XDG_CONFIG_HOME:-$HOME/.config}/yacsc/yacsc.d/*`

Configuration files are split into three sections
- `global` - Configuration options that are not explicitly related the other sections
- `colours` - General reusable colour definitions
- `schemas` - Collections of regular expressions and colours used to colourise text

### Global
- `default_schema` - Defines the colour default schema that is used when no explicit schema is selected via the `--schema` command argument
- `max_batched_lines` - Defines the maximum number of lines of input to batch for parallel processing default `1000`

### Colours
The colours section is defined as a toml table contains key value pairs of colour names to colour definitions, for example
```toml
[colours]
info = "term_blue"
warn = "#ffa500"
red = "#FF0000"
bold_red = {fg = "#000000", bg = "term_red", bold = true}
```
Colours can be defined in three ways as hex colours, standard terminal defined colours via `term_` prefixes such as `term_red` and as a styled colour containing foreground, background and other style information.

#### Supported Term colours:
- `term_black`
- `term_red`
- `term_green`
- `term_yellow`
- `term_blue`
- `term_magenta`
- `term_cyan`
- `term_white`
- `term_black_alt`
- `term_red_alt`
- `term_green_alt`
- `term_yellow_alt`
- `term_blue_alt`
- `term_magenta_alt`
- `term_cyan_alt`
- `term_white_alt`

#### Styled colours:
Styled colours are defined as a separate toml table of the following format:
- `fg` - The foreground colour
- `bg` - The background colour (Optional)
- `bold` - boolean indicating if the text should be bold (Optional)
- `dimmed` - boolean indicating if the text should be dimmed (Optional)
- `italic` - boolean indicating if the text should be italic (Optional)
- `underline` - boolean indicating if the text should be underlined (Optional)
- `blink` - boolean indicating if the text should blink (Optional)
- `reverse` - boolean indicating if the texts colours should be reversed (Optional)
- `hidden` - boolean indicating if the text should be hidden (Optional)
- `strikethrough` - boolean indicating if the text should be struck through (Optional)

### Schemas
The schemas section is defined as a map of name schemas to an array of colour rules which are applied in the order that they are defined. Currently two types of colour rules are supported regular expressions and text surrounds.

#### Regular Expression colour rules
Regular expression colour rules can be used to find and colourise text using a regular expression and are defined within a particular schema with three fields
- `type = "regex"` - indicates that this rule is for a regular expression
- `pattern` - A regular expression string defined in the [rust regex syntax](https://docs.rs/regex/latest/regex/#syntax)
- `colour` - A colour as defined in the colours section above, as one of the keys defined in the colours section or if the regex contains capture groups a table of key value pairs indicating capture group and colour.

Example regex rule
```toml
[[schemas.regex-rule-name]]
type = "regex"
pattern = '[0-9]+(\.[0-9]+)*'
colour = "term_yellow"
```

#### Text Surround rule
The text surround rule can be used to colourise text that is surrounded by two characters and is defined within a particular schema by the following fields
- `type = "surround"` - Indicates that this rule is a surround text rule
- `start` - The character to indicate colouring start
- `escape` - An optional character to indicate an escaped end character
- `end` - The character to indicate colouring end
```toml
[[schemas.simple]]
type = "surround"
start = "\""
end = "\""
escape = "\\"
colour = "term_magenta"
```

### Example configuration
```toml
default_schema = "schema-one"

[colours]
info = "#1DEDCD"
warn = "#ffa500"
red = "#FF0000"
bold_red = {fg = "#000000", bg = "term_red", bold = true}

[[schemas.schema-one]]
type = "surround"
start = "\""
end = "\""
escape = "\\"
colour = "term_magenta"

[[schemas.schema-one]]
type = "regex"
pattern = "\\berror\\b"
colour = "red"

[[schemas.schema-one]]
type = "regex"
pattern = "\\binfo\\b"
colour = "info"

[[schemas.schema-one]]
type = "regex"
pattern = "\\bwarn\\b"
colour = "warn"

[[schemas.schema-two]]
type = "regex"
pattern = '(?P<usage>Usage:)\s(?P<command>[^\s]+)\s(?P<options>(?:\[.*?\]\s?)*)\s(?P<positionals>(?:[A-Z]+(?:\.\.\.)?)*)'
colour = {usage = {fg = "info", bold = true}, command = "term_green", options = "term_blue", positionals = "term_cyan"}
```

## Building
Yacsc is written in rust as such you will need a rust installation in order to compile it.

To build yacsc:
```
git clone https://gitlab.com/LockSham/yacsc
cd yacsc
cargo build --release

./target/release/yacsc --help
```

### Release building
Build release packages and tarballs can be created by running the `./scripts/build_release.sh` script which will create a versioned source code archive, statically compiled binary release tarball and a statically compiled binary debian package in `./release/<version>`.

Note building the debian package requires the `cargo-deb` cargo subcommand see [cargo-deb](https://github.com/kornelski/cargo-deb#installation) for installation instructions.
