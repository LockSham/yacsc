#!/bin/sh
# Yacsc (Yet Another CLI Stream Colouriser) - A regular expression cli stream colouriser
# Copyright (C) 2023  Lachlan Shambrook
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -eu

script_dir="$(cd "$(dirname "$(readlink -f "$0")")" && pwd)"
project_dir="$(readlink -f "$script_dir/..")"

version="$("$project_dir/scripts/get_release_version.sh")"
package_version="$(echo "$version" | sed -E 's/^v([[:digit:]]*\.[[:digit:]]*\.[[:digit:]]*)/\1/')"
artifacts_dir="$project_dir/artifacts/$version"

build_target() {
	RUSTFLAGS="--remap-path-prefix $project_dir/= -C target-feature=+crt-static" \
		cargo build --release --target="$1"
	cargo deb \
		--target="$1" \
		--no-build \
		--deb-version "$package_version" \
		--output "$artifacts_dir"
}

build_and_archive_target() {
	staging_dir="$artifacts_dir/staging"

	build_target "$1"

	rm -rf "$staging_dir"
	mkdir -p "$staging_dir"
	cp -r \
		"$project_dir/examples" \
		"$project_dir/README.md" \
		"$project_dir/COPYING" \
		"$project_dir/target/$1/release/yacsc" \
		"$staging_dir"
	tar czf "$artifacts_dir/yacsc_${package_version}_${1}.tar.gz" -C "$staging_dir" .

	rm -rf "$staging_dir"
}

build_yacsc() {
	rm -rf "$artifacts_dir"
	mkdir -p "$artifacts_dir"
	build_and_archive_target "x86_64-unknown-linux-gnu"
}

build_yacsc

echo "Build $version built in $artifacts_dir"
