#!/bin/sh
set -eu

tag="$(git tag \
	--list 'v[0-9]*\.[0-9]*\.[0-9]*' \
	--sort -v:refname \
	--points-at HEAD \
	| head -n 1
)"

if [ -n "$tag" ]; then
	echo "$tag"
	exit 0
fi

last_tag="$(git tag --list --merged HEAD --sort -v:refname | head -n 1)"
rev="$(git rev-parse --short HEAD)"

echo "$last_tag-$rev"
